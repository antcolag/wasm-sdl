#!/bin/sh
mkdir -p build
emcc main.cc --bind -Wall -Wextra -std=gnu++2b -pedantic-errors -sUSE_SDL=2 -D_REENTRANT -lSDL2 $@
